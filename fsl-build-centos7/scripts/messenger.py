#!/usr/bin/env python
import os
import slack
import argparse

# setup command line options
parser = argparse.ArgumentParser(
    description="send a message to slack when gitlab runs FSL build steps")

parser.add_argument(
    '--message',
    help="The message to send to the #fsl-build slack channel",
    type=str
)


args = parser.parse_args()
client = slack.WebClient(token=os.environ['SLACK_API_TOKEN'])

response = client.chat_postMessage(
    channel='#fsl-builds',
    text=args.message
    )
assert response["ok"]
#assert response["message"]["text"] == args.message