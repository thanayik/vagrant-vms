#!/bin/bash

set -e 

#______________________________________________________________________________
#
#                     scripts setup
#______________________________________________________________________________
# copy over cuda rpms and fslinstall.py from host
sudo cp -R /home/vagrant/scripts /root/scripts

#______________________________________________________________________________
#
#                     switch to vagrant user directory 
#______________________________________________________________________________
cd /home/vagrant

#______________________________________________________________________________
#
#                     OS setup
#______________________________________________________________________________
# sudo yum clean all
# sudo rm -rf /var/cache/yum/*

# add oxford mirror because centos default mirror was broken from Sep 18, 2019 (suspiciously after centos 7.7 released)
# sudo yum-config-manager --add-repo=http://mirror.ox.ac.uk/sites/mirror.centos.org/7.6.1810/os/x86_64
# update centos packages
sudo yum -y update

# install the software we need
sudo yum install -y epel-release
sudo yum groupinstall -y "Development Tools"
sudo yum install -y \
        parallel \
        libX11-devel \
        bzip2 \
        bc \
        file \
        libpng12 \
        libmng \
        openblas-devel \
        sudo \
        wget \
        which \
        xeyes \
        zlib-devel \
        openssh \
        openssh-server \
        openssh-clients \
        openssh-libs \
        expat-devel \
        curl \
        git \
        cvs \
        libXcomposite \
        libXcursor \
        libXi \
        libXtst \
        libXrandr \
        alsa-lib \
        mesa-libEGL \
        mesa-libGL-devel \
        mesa-libGLU-devel-9.0.0-4.el7.x86_64 \
        libXdamage \
        mesa-libGL \
        libXScrnSaver

#______________________________________________________________________________
#
#                     python conda setup
#______________________________________________________________________________
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /home/vagrant/miniconda.sh
/bin/bash /home/vagrant/miniconda.sh -b -p /home/vagrant/miniconda
export PATH="/home/vagrant/miniconda/bin:$PATH"
echo "export PATH="/home/vagrant/miniconda/bin:$PATH"" >> /home/vagrant/.bashrc
# install paramiko and pyyaml for FSL repository checkout procedure later
conda install -y -c anaconda paramiko pyyaml pip
pip install slackclient
# put messenger.py in /usr/bin/
sudo cp /home/vagrant/scripts/messenger.py /usr/bin/messenger.py
sudo chmod +x /usr/bin/messenger.py

#______________________________________________________________________________
#
#                     CUDA setup
#______________________________________________________________________________
# install minimal cuda 
sudo rpm -i /home/vagrant/scripts/cuda-repo-rhel7-10.1.243-1.x86_64.rpm
sudo yum -y install cuda-minimal-build-8-0  
sudo yum -y install cuda-minimal-build-9-1  
sudo yum -y install cuda-minimal-build-10-0  

#______________________________________________________________________________
#
#                     VTK setup
#______________________________________________________________________________
# rm -rf vtk*
# wget -qO- https://git.fmrib.ox.ac.uk/fsl/vtk/-/archive/master/vtk-master.tar.gz | tar -xzf -
# mv vtk-master vtk

#______________________________________________________________________________
#
#                     CVS setup
#______________________________________________________________________________
# set up CVS stuff for some legacy repos that need to be checked out for builds
echo "export CVS_RSH=`which ssh`" >> /home/vagrant/.profile
echo "export CVSROOT=:ext:thanayik@jalapeno.fmrib.ox.ac.uk:/usr/local/share/sources" >> /home/vagrant/.bashrc

#______________________________________________________________________________
#
#                     PATHS setup
#______________________________________________________________________________
# add /usr/local/bin to path for gitlab-runner
# echo "export PATH=$PATH:/usr/local/bin" >> /home/vagrant/.profile

#______________________________________________________________________________
#
#                      # souce profile
#______________________________________________________________________________
# source /home/vagrant/.profile

# cd $HOME

#______________________________________________________________________________
#
#                     Gitlab runner setup
#______________________________________________________________________________

# gitlab runner setup reference material can be found here:
# https://docs.gitlab.com/runner/install/linux-manually.html
# install gitlab-runner in the VM so artefacts can be uploaded after builds
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# give gitlab runner execute permission
sudo chmod +x /usr/local/bin/gitlab-runner

# create a gitlab-runner user
# sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# install and run as a service
sudo /usr/local/bin/gitlab-runner install --user=vagrant --working-directory=/home/vagrant
sudo /usr/local/bin/gitlab-runner start

# unregister all runners created by this machine
sudo /usr/local/bin/gitlab-runner unregister --name fsl-build-centos7

# now register a new runner (this runner will be accessible as long as the VM is running)
sudo /usr/local/bin/gitlab-runner register \
--non-interactive \
--name fsl-build-centos7 \
--url "https://git.fmrib.ox.ac.uk/" \
--registration-token "9yWBA8H77FxZCaHgrnD7" \
--executor "shell" \
--description "Centos 7 VM (shell)" \
--tag-list "centos7" \
--run-untagged="false" \
--locked="false"


