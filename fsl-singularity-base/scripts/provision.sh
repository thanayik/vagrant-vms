#!/bin/bash

set -e 

#______________________________________________________________________________
#
#                     scripts setup
#______________________________________________________________________________
# copy over cuda rpms and fslinstall.py from host
WHO=`whoami`
sudo cp -R /home/$WHO/scripts /root/scripts

#______________________________________________________________________________
#
#                     switch to vagrant user directory 
#______________________________________________________________________________
cd /home/$WHO

#______________________________________________________________________________
#
#                     OS setup
#______________________________________________________________________________
# update centos 
yum -y update

# install the software we need
yum install -y epel-release
yum groupinstall -y "Development Tools"
yum install -y \
        parallel \
        libX11-devel \
        bzip2 \
        bc \
        file \
        libpng12 \
        libmng \
        openblas-devel \
        sudo \
        wget \
        which \
        xeyes \
        zlib-devel \
        octave \
        openssh \
        openssh-server \
        openssh-clients \
        openssh-libs \
        expat-devel \
        curl \
        git \
        cvs \
        libXcomposite \
        libXcursor \
        libXi \
        libXtst \
        libXrandr \
        alsa-lib \
        mesa-libEGL \
        libXdamage \
        mesa-libGL \
        libXScrnSaver

#______________________________________________________________________________
#
#                     python conda setup
#______________________________________________________________________________
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /home/$WHO/miniconda.sh
/bin/bash /home/$WHO/miniconda.sh -b -p /home/$WHO/miniconda
export PATH="/home/$WHO/miniconda/bin:$PATH"
echo "export PATH="/home/$WHO/miniconda/bin:$PATH"" >> /home/$WHO/.bashrc
# install paramiko and pyyaml for FSL repository checkout procedure later
conda install -y -c anaconda paramiko pyyaml

#______________________________________________________________________________
#
#                     CUDA setup
#______________________________________________________________________________
# install minimal cuda 
rpm -i /home/$WHO/scripts/cuda-repo-rhel7-10.0.130-1.x86_64.rpm
yum -y install cuda-minimal-build-8-0  
yum -y install cuda-minimal-build-9-1  
yum -y install cuda-minimal-build-10-0  

#______________________________________________________________________________
#
#                     VTK setup
#______________________________________________________________________________
rm -rf vtk*
wget -qO- https://git.fmrib.ox.ac.uk/fsl/vtk/-/archive/master/vtk-master.tar.gz | tar -xzf -
mv vtk-master vtk

#______________________________________________________________________________
#
#                     CVS setup
#______________________________________________________________________________
# set up CVS stuff for some legacy repos that need to be checked out for builds
echo "export CVS_RSH=`which ssh`" >> /home/$WHO/.profile
echo "export CVSROOT=:ext:thanayik@jalapeno.fmrib.ox.ac.uk:/usr/local/share/sources" >> /home/$WHO/.bashrc

#______________________________________________________________________________
#
#                     PATHS setup
#______________________________________________________________________________
# add /usr/local/bin to path for gitlab-runner
# echo "export PATH=$PATH:/usr/local/bin" >> /home/vagrant/.profile

#______________________________________________________________________________
#
#                      # souce profile
#______________________________________________________________________________
# source /home/vagrant/.profile

# cd $HOME

#______________________________________________________________________________
#
#                     Gitlab runner setup
#______________________________________________________________________________

# gitlab runner setup reference material can be found here:
# https://docs.gitlab.com/runner/install/linux-manually.html
# install gitlab-runner in the VM so artefacts can be uploaded after builds
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# give gitlab runner execute permission
chmod +x /usr/local/bin/gitlab-runner

# create a gitlab-runner user
# sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# install and run as a service
/usr/local/bin/gitlab-runner install --user=$WHO --working-directory=/home/$WHO
/usr/local/bin/gitlab-runner start

# unregister all runners created by this machine
/usr/local/bin/gitlab-runner unregister --all-runners

# now register a new runner (this runner will be accessible as long as the VM is running)
/usr/local/bin/gitlab-runner register \
--non-interactive \
--url "https://git.fmrib.ox.ac.uk/" \
--registration-token "X4gDzPa67cZMo_yXsTdc" \
--executor "shell" \
--description "Centos 7 VM (shell)" \
--tag-list "centos7" \
--run-untagged="false" \
--locked="false"


